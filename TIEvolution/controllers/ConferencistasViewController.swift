//
//  ConferencistasViewController.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class ConferencistasViewController: UIViewController, TableViewCellClickDelegate {
    
    @IBOutlet weak var conferencistasTableView: UITableView!
    
    var mConferencistasDataSource : BaseDataSource<Conferencista, ConferencistaTableViewCell>!

    override func viewDidLoad() {
        super.viewDidLoad()

        var conferencistas : [Conferencista] = []
        conferencistas.append(Conferencista(imagen: UIImage(named: "kuri")!, nombre: "Eduardo Kuri", descripcion: "Egresado de la Universidad La Salle como Ingeniero en Electrónica y con posgrado en IPADE Business School, el Director General de Totalplay cuenta con más de 20 años de experiencia en la industria de Telecomunicaciones, trabajando para TV Azteca y Iusacell, empresas pertenecientes al Grupo Salinas."))
        conferencistas.append(Conferencista(imagen: UIImage(named: "aurelio")!, nombre: "Aurelio Saynes", descripcion: "Egresado de UPIICSA del Instituto Politécnico Nacional, con una maestría en Dirección por el IPADE Business School, nuestro Director de Sistema de Totalplay desde hace cuatro años, cuenta con más de 15 años de experiencia en las áreas de sistemas en empresas como Banco Azteca y Afore Azteca"))
        conferencistas.append(Conferencista(imagen: UIImage(named: "samuel")!, nombre: "Samuel Lee Belmonte", descripcion: "Ingeniero en Sistemas Electrónicos ITESM Management of Technological Change MIT One to One Marketing Kellog Univ Course for Presidents and CEOs American Management Association"))
        conferencistas.append(Conferencista(imagen: UIImage(named: "hector")!, nombre: "Héctor Nava", descripcion: "Egresado del Tecnológico de Monterrey como Ingeniero en Electrónica y Comunicaciones, con un MBA en Administración de Negocios de la Universidad de Phoenix, el Director General de Totalplay Empresarial cuenta con más de 20 de experiencia en la industria de Telecomunicaciones, trabajando en empresas como Nokia."))
        conferencistas.append(Conferencista(imagen: UIImage(named: "nestor")!, nombre: "Néstor Márquez", descripcion: "Licenciado en Análisis de Sistemas por la Universidad Católica de La Plata, Argentina, con un MBA por el Instituto para el Desarrollo Empresarial de Argentina y un Posgrado en Comunicación Institucional de la Universidad de Ciencias Empresariales y Sociales, este Profesor del TEC de Monterrey ha sido conferencias en varios países de Latinoamérica, Estados Unidos, Canadá, España y Londres."))
        conferencistas.append(Conferencista(imagen: UIImage(named: "jose_marquez")!, nombre: "José Luis Rodríguez", descripcion: "Graduado de la facultad de ingeniera en la UNAM; Maestría en Ingeniería Eléctrica en la Universidad de Stanford. Cuenta con más de 20 años de experiencia en Telco, ha trabajado en empresas como Silicon Graphics, BEA, actualmente es Director de Telecomunicaciones en Grupo Salinas y CTO de Totalplay Coporativo"))
        
        mConferencistasDataSource = BaseDataSource(tableView: conferencistasTableView, delegate : self)
        mConferencistasDataSource.update(items: conferencistas)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DetalleConferencistaViewController = storyboard.instantiateViewController(withIdentifier: "DetalleConferencistaViewController") as! DetalleConferencistaViewController
        vc.conferencista = item as? Conferencista
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
