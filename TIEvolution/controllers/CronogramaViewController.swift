//
//  ViewController.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 13/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class CronogramaViewController: UIViewController {

    @IBOutlet weak var mCronogramaTableView: UITableView!
    var mCronogramaDataSource : BaseDataSource<EventoCronograma, ItemCronogramaViewTableViewCell>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var eventos : [EventoCronograma] = []
        
        eventos.append(EventoCronograma(hora: "8:30 - 9:00", titulo: "Recepción", direccion : "Lobby del Auditorio"))
        eventos.append(EventoCronograma(hora: "9:00 - 9:10", titulo: "Apertura del Evento", direccion : "Aurelio Saynes"))
        eventos.append(EventoCronograma(hora: "9:10 - 9:20", titulo: "Mensaje del Director General Corporativo ", direccion : "Eduardo Kuri"))
        eventos.append(EventoCronograma(hora: "9:20 - 10:00", titulo: "Nueva visión Recursos Humanos", direccion : "Guillermo Villaseñor"))
        eventos.append(EventoCronograma(hora: "10:00 - 10:20", titulo: "Sistemas Grupo Salinas", direccion : "Manuel González"))
        eventos.append(EventoCronograma(hora: "10:20 - 10:40", titulo: "Visión Totalplay", direccion : "Samuel Lee"))
        eventos.append(EventoCronograma(hora: "10:40 - 11:00", titulo: "Visión Totalplay Empresarial", direccion : "Héctor Nava"))
        eventos.append(EventoCronograma(hora: "11:00 - 11:20", titulo: "Red Central ", direccion: "José Luis Rodriguez"))
        eventos.append(EventoCronograma(hora: "11:20 - 11:45", titulo: "Bienestar del Personal", direccion : "Ivette Arellano"))
        eventos.append(EventoCronograma(hora: "11:45 - 12:00", titulo: "Break", direccion : "Lobby del auditorio"))
        eventos.append(EventoCronograma(hora: "12:00 - 14:00", titulo: "Conferencia Magistral", direccion : "\"Transformación Digital y Disrupción\""))
        eventos.append(EventoCronograma(hora: "14:00 - 16:00", titulo: "Comida", direccion : "Salon Velaria"))
        eventos.append(EventoCronograma(hora: "16:00 - 16:45", titulo: "Continiación Conferencia Magistral", direccion : "\"Transformación Digital y Disrupción\""))
        eventos.append(EventoCronograma(hora: "16:45 - 17:00", titulo: "Break", direccion : ""))
        eventos.append(EventoCronograma(hora: "17:00 - 18:00", titulo: "Estrategia TI", direccion : "Aurelio Saynes"))
        eventos.append(EventoCronograma(hora: "18:00 - 20:00", titulo: "Coctél", direccion : ""))
        
        mCronogramaDataSource = BaseDataSource(tableView: mCronogramaTableView)
        mCronogramaDataSource.update(items: eventos)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

