//
//  DetalleConferencistaViewController.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class DetalleConferencistaViewController: UIViewController {
    
    @IBOutlet weak var mImagenConferencista: UIImageView!
    @IBOutlet weak var mNombreLabel: UILabel!
    @IBOutlet weak var mDescripcionLabel: UILabel!
    
    var conferencista : Conferencista?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mImagenConferencista.clipsToBounds = true
        mImagenConferencista.layer.cornerRadius = 108
        
        mImagenConferencista.image = conferencista?.imagen
        mNombreLabel.text = conferencista?.nombre
        mDescripcionLabel.text = conferencista?.descripcion

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
