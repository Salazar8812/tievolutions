//
//  MainViewController.swift
//  TIEvolution
//
//  Created by Charls Salazar on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let userDef = UserDefaults.standard
        
        
        if (userDef.string(forKey: "isFirstTime") == nil)
        {
            userDef.set("true", forKey: "isFirstTime")
            print("Take Selfie")
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: CaptureSelfieViewController = storyboard.instantiateViewController(withIdentifier: "CaptureSelfieViewController") as! CaptureSelfieViewController
            self.navigationController?.present(vc, animated: true)
            
        }else{
            print("Do Nothing")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
