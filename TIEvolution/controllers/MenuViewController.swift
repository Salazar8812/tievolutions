//
//  MenuViewController.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 13/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit
import SideMenu
import AVFoundation

class MenuViewController: UIViewController {
    
    @IBOutlet weak var mProfileImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        mProfileImageView.layer.borderWidth = 1
        mProfileImageView.layer.masksToBounds = false
        mProfileImageView.layer.borderColor = UIColor.white.cgColor
        mProfileImageView.layer.cornerRadius = mProfileImageView.frame.height/2
        mProfileImageView.clipsToBounds = true
        
//        SideMenuManager.default.menuLeftNavigationController = self
        //Initialize session an output variables this is necessary
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("selfie.png")
            mProfileImageView.image = UIImage(contentsOfFile: imageURL.path)
        }
    }

    @IBAction func mProfileButton(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CaptureSelfieViewController = storyboard.instantiateViewController(withIdentifier: "CaptureSelfieViewController") as! CaptureSelfieViewController
        self.navigationController?.present(vc, animated: true)
    }
    
    @IBAction func onPrincipalClick(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
