//
//  UbicacionViewController.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit
import MapKit

class UbicacionViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let span = MKCoordinateSpanMake(0.004, 0.004)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 19.2913829, longitude: -99.1653844), span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 19.2913829, longitude: -99.1653844)
        mapView.addAnnotation(annotation)
    }
    
    @IBAction func onRouteClick(_ sender: Any) {
        let directionsURL = "http://maps.apple.com/?saddr=Current%20Location&daddr=19.2913829,-99.1653844"
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
