//
//  CaptureSelfieViewController.swift
//  TIEvolution
//
//  Created by Charls Salazar on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//
import UIKit
import AACameraView

class CaptureSelfieViewController: UIViewController {
    
    @IBOutlet weak var mAACameraView: AACameraView!
    var capturedImage: UIImage?
    
    override func viewDidLoad() {
        mAACameraView.response = { response in
            if let img = response as? UIImage {
                self.capturedImage = img
                self.dismiss(animated: true, completion: nil)
                
                if let data = UIImagePNGRepresentation(self.capturedImage!) {
                    let fm = FileManager.default
                    let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    let filename = docsurl.appendingPathComponent("selfie.png")
                    try? data.write(to: filename)
                }
            }
            else if let error = response as? Error {
                print("Error: ", error.localizedDescription)
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mAACameraView.startSession()
        
    }
    
    @IBAction func mFlasButton(_ sender: Any) {
        mAACameraView.toggleFlash()
    }
    
    
    @IBAction func mCaptureButton(_ sender: Any) {
        mAACameraView.triggerCamera()
    }
    
    @IBAction func mFlipCameraButton(_ sender: Any) {
        mAACameraView.toggleCamera()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
