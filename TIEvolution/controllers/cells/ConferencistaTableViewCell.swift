//
//  ConferencistaTableViewCell.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class ConferencistaTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var nombre: UILabel!
    

    override func pupulate(object :NSObject) {
        let item : Conferencista = object as! Conferencista
        ImageView.image = item.imagen
        nombre.text = item.nombre
    }
    
    override func toString() -> String{
        return "ConferencistaTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ImageView.layer.cornerRadius = 40
        ImageView.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
