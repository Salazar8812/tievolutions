//
//  ItemCronogramaViewTableViewCell.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 13/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class ItemCronogramaViewTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var mHourLabel: UILabel!
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mCheckBoxButton: CheckBoxButton!
    
    @IBOutlet weak var mDireccionView: UIView!
    @IBOutlet weak var mDirrecionValueLabel: UILabel!
    
    var item : EventoCronograma! = nil
    
    override func pupulate(object :NSObject) {
        item = object as! EventoCronograma
        mHourLabel.text = item.hora
        mTitleLabel.text = item.titulo
        mDirrecionValueLabel.text = item.direccion
        if item.direccion == "" {
            ViewUtils.setVisibility(view: mDireccionView, visibility: .GONE)
        } else {
            ViewUtils.setVisibility(view: mDireccionView, visibility: .NOT_GONE)
        }
        mCheckBoxButton.isChecked = item.checked
    }
    
    @IBAction func onClick(_ sender: CheckBoxButton) {
        item.checked = sender.isChecked
    }
    
    override func toString() -> String{
        return "ItemCronogramaViewTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
