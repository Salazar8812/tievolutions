//
//  Conferencista.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 14/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class Conferencista: NSObject {
    
    var imagen : UIImage!
    var nombre : String = ""
    var descripcion : String = ""
    
    init(imagen : UIImage, nombre : String, descripcion : String){
        self.imagen = imagen
        self.nombre = nombre
        self.descripcion = descripcion
    }

}
