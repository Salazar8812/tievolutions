//
//  EventoCronograma.swift
//  TIEvolution
//
//  Created by Jorge Hdez VIlla on 13/11/17.
//  Copyright © 2017 TotalPlay. All rights reserved.
//

import UIKit

class EventoCronograma: NSObject {
    
    var hora : String = ""
    var titulo : String = ""
    var direccion : String = ""
    var checked : Bool = false
    
    init(hora : String, titulo : String, direccion : String) {
        super.init()
        self.hora = hora
        self.titulo = titulo
        self.direccion = direccion
    }

}
